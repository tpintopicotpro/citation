import { Application } from "express";
import AdminController from "./controllers/AdminController";

import AuteurController from "./controllers/AuteurController";
import CitationsController from "./controllers/CitationsController";
import HomeController from "./controllers/HomeController";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/auteur-create', (req, res) => {
        HomeController.showForm_auteur(req, res)
    });

    app.post('/auteur-create', (req, res) => {
        AuteurController.add_auteur(req, res)
    });

    app.get('/auteur-read/:id', (req, res) => {
        AuteurController.read_one(req, res)
    });

    app.get('/auteur-read', (req, res) => {
        AuteurController.readAll(req, res)
    });


    app.get('/auteur-delete/:id', (req, res) => {
        AuteurController.delete(req, res)
    })

    app.get('/auteur-update/:id', (req, res) => {
        AuteurController.showFormUpdate(req, res)
    });


    app.post('/auteur-update/:id', (req, res) => {
        AuteurController.update(req, res)
    });


    app.get('/citation-create', (req, res) => {
        HomeController.showForm_citation(req, res)
    });

    app.post('/citation-create', (req, res) => {
        CitationsController.add_citation(req, res)
    });
    app.get('/citation-update/:id', (req, res) => {
        AdminController.showFormUpdate_citations(req, res)
    });

    app.post('/citation-update/:id', (req, res) => {
        CitationsController.update(req, res)
    });

    app.get('/citation-read/:id', (req, res) => {
        CitationsController.read(req, res)
    });

    app.get('/citations/:id' , (req, res) => {
        CitationsController.readAll(req, res)
    })

    app.get('/admin/citations/:id', (req, res) => {
        AdminController.readAll_citations(req, res)
    })


    app.get('/admin/auteur', (req, res) => {
        AdminController.readAll(req, res)
    })

    app.get('/admin', (req, res)=> {
        res.render('pages/admin')
    })
    // app.get('/citation-create', (req, res) =>
    // {
    //     AuteurController.delete(req, res)
    // });
}
