-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Thomas
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 09:55
-- Created:       2022-02-21 09:39
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "auteur"(
  "id" INTEGER PRIMARY KEY NOT NULL,
  "name" VARCHAR(45)
);
CREATE TABLE "citations"(
  "id" INTEGER PRIMARY KEY NOT NULL,
  "content" LONG VARCHAR NOT NULL,
  "auteur_id" INTEGER NOT NULL,
  CONSTRAINT "fk_citations_auteur"
    FOREIGN KEY("auteur_id")
    REFERENCES "auteur"("id")
);
CREATE INDEX "citations.fk_citations_auteur_idx" ON "citations" ("auteur_id");
COMMIT;


