import { Request, Response } from "express-serve-static-core";
import HomeController from "./HomeController";


export default class CitationsController {


    static add_citation(req: Request, res: Response): void
{
    const db = req.app.locals.db;
    
    console.log("mon auteur:",req.body.citations_auteur)
    console.log("mon content:",req.body.content)
    db.prepare('INSERT INTO citations ("content", "auteur_id") VALUES (?, ?)').run(req.body.content, req.body.citations_auteur);
    //arr_auteur.push(req.body.citations_auteur)
    res.redirect('citations/1')
}

static update(req: Request, res: Response)
{
    const db = req.app.locals.db;
    console.log('mon reqbody title:', req.body.title)
    console.log('mon reqbody content:', req.body.citations_auteur)
    console.log(req.params)
    db.prepare('UPDATE citations SET content = ?, auteur_id = ? WHERE id = ?').run(req.body.title, req.body.citations_auteur, req.params.id);


}

static read(req: Request, res: Response): void
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    let id = citations.auteur_id
    let arr = auteur.find((element: any) => element.id == id)
    //console.log("arr", arr)
    //let isFounded = citations.some( elm => auteur.includes(elm) )

    //console.log("lalda",citations)
    res.render('pages/citations-read', {
        citations: citations,
        auteur_name : arr.name
        
    });
}


static read_one(req: Request, res: Response): void
{

    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    let id = citations.auteur_id
    let arr = auteur.find((element: any) => element.id == id)
    console.log("arr id :",arr.id)
    const all_citations = db.prepare('SELECT * FROM citations WHERE auteur_id = ?').all(arr.id)
    console.log("toute mes citat:",all_citations)
    console.log("mon arr:",arr)
    res.render('pages/citations-read', {
        citations: all_citations,
        auteur_name: arr.name
    });
}
static showFormUpdate_citations(req: Request, res: Response)
{
    const db = req.app.locals.db;

    const auteur = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    console.log(auteur)
    res.render('pages/citation-update', {
        content: auteur.content
    });
}

static readAll(req: Request, res: Response)
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const cita = db.prepare('SELECT * FROM citations').all()
    let num = 10
    let requ = +req.params.id
    let id = ((num * requ)-10)     
    // console.log("debut du req", id)
    // console.log("mon req params:",requ)
    const citations = db.prepare('SELECT * FROM citations LIMIT 10 OFFSET ?').all(id);
    //console.log("citat:",cita.length)
    let longueur = Math.ceil(((cita.length/10)))
    //console.log("ma limité:",longueur)
    
    if(requ >= longueur){
        requ = longueur-1
    }
    //citations.filter((element: any) => element.auteur_id === auteur.id)
    // if (citations.length % 10 > 1){
    //     document.createElement= `<button>Page Suivante</button>`
    // }
    //let isFounded = citations.some( elm => auteur.includes(elm) )

    // console.log("lalda",citations)
    // console.log("auteur:" , auteur)
    res.render('pages/citations', {
        citations: citations,
        auteur: auteur,
        id : requ
    });
}
}
