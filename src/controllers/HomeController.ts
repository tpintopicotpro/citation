import { Request, Response } from "express-serve-static-core";
let arr_auteur = []
export default class HomeController
{
    static index(req: Request, res: Response)
    {   let array: any[] = []
        const db = req.app.locals.db;
        const aut = db.prepare('SELECT * FROM auteur').all()
        //console.log("mon aut:", aut)
        
        
        const pre_aut = db.prepare('SELECT * FROM citations').all();
        let id = pre_aut.length
        const auteur = pre_aut.slice(-1)
        //console.log("la:",auteur)
        let auteur_name = aut.filter((element: { id: any; }) => element.id === auteur[0].auteur_id)

        res.render('pages/index', {
            title: 'La derniere citation:',
            auteur: auteur,
            auteur_name: auteur_name
        });
    }


    static citations(req: Request, res: Response): void
    {   let id
        
        const db = req.app.locals.db;
        const auteur = db.prepare('SELECT * FROM auteur').all();
        const citations = db.prepare('SELECT * FROM citations').all();
        // if (auteur.id === citations.auteur_id){
        //     return id
        // }

        res.render('pages/citations', {
            title: 'About',
            citations: citations,
            id: id
        });
    }

    /**
     * Affiche le formulaire de creation d'article
     * @param req 
     * @param res 
     */
     static showForm_auteur(req: Request, res: Response): void
     {
         res.render('pages/auteur-create');
     }

     static showForm_citation(req: Request, res: Response): void
     {  
         const db = req.app.locals.db;
         let auteur = db.prepare("SELECT * FROM auteur").all()
        
         res.render('pages/citation-create', {
            auteur: auteur
         })
     }
}