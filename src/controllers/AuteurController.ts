import { Request, Response } from "express-serve-static-core";
import HomeController from "./HomeController";

export default class AuteurController {

static add_auteur(req: Request, res: Response): void
{
    const db = req.app.locals.db;

    db.prepare('INSERT INTO auteur ("name") VALUES (?)').run(req.body.name);

    HomeController.index(req, res);
}
static read(req: Request, res: Response): void
{
    const db = req.app.locals.db;

    const auteur = db.prepare('SELECT * FROM citations WHERE auteur_id = ?').all(req.params.id);
    console.log(auteur)
    res.render('pages/auteur', {
        auteur: auteur
    });
}



static read_one(req: Request, res: Response): void
{

    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    let id = citations.auteur_id
    let arr = auteur.find((element: any) => element.id == req.params.id)
    console.log("arr id :",arr.id)
    const all_citations = db.prepare('SELECT * FROM citations WHERE auteur_id = ?').all(req.params.id)
    console.log("toute mes citat:",all_citations)
    console.log("mon arr:",arr)
    res.render('pages/auteur-all', {
        auteur: all_citations,
        auteur_name: arr.name,
    });
}

static showFormUpdate(req: Request, res: Response)
{
    const db = req.app.locals.db;

    const auteur = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);

    res.render('pages/auteur-update', {
        auteur: auteur
    });
}

static delete (req: Request, res: Response): void 
{
    const db = req.app.locals.db;
    console.log(req.params.id) 
    db.prepare('DELETE FROM citations WHERE auteur_id = ?').run(req.params.id)
    db.prepare('DELETE FROM auteur WHERE id=?').run(req.params.id)
   res.redirect('/auteur-read')
}

static update (req: Request, res: Response) : void 
{
    const db = req.app.locals.db;
    const auteur = db.prepare('UPDATE auteur SET name = ? WHERE id = ?').run(req.body.name, req.params.id)
    res.redirect('/auteur-read')
}

static readAll(req: Request, res: Response): void
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations').all();
    let id: any[] = []
    let myAut = []
    for(let i=0; i<citations.length; i++){
        if(id.includes(citations[i].auteur_id)){

        }
        else{
        id.push(citations[i].auteur_id)
        }
    }
    

    for (let i = 0; i<auteur.length ; i++){
        // console.log('mon arr:',auteur[i].id)
        // console.log("mon id:", id)
        if(id.includes(auteur[i].id)){
            myAut.push(auteur[i])
        }
        else{
            
        }
    }
    console.log("monAut:",myAut)
    
    //citations.filter((element: any) => element.auteur_id === auteur.id)
   
    //let isFounded = citations.some( elm => auteur.includes(elm) )

    res.render('pages/auteur', {
        auteur: myAut,
        title: "Voici tous les auteurs ayant une citation enregistrée:"
        
    });
}



}
