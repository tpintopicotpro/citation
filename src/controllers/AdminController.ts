import { Request, Response } from "express-serve-static-core";

export default class AdminController {

static read_one(req: Request, res: Response): void
{

    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    let id = citations.auteur_id
    let arr = auteur.find((element: any) => element.id == id)
    console.log("arr id :",arr.id)
    const all_citations = db.prepare('SELECT * FROM citations WHERE auteur_id = ?').all(arr.id)
    console.log("toute mes citat:",all_citations)
    console.log("mon arr:",arr)
    res.render('pages/citations-read', {
        citations: all_citations,
        auteur_name: arr.name
    });
}

static showFormUpdate_citations(req: Request, res: Response)
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const citations = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
    let id = citations.auteur_id
    let arr = auteur.find((element: any) => element.id == id)
    console.log("mon content:",citations)
    console.log("mon auteur-name:", arr);
    
    res.render('pages/citation-update', {
        content:citations,
        auteur : arr
    });
}

static readAll_citations(req: Request, res: Response)
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    const cita = db.prepare('SELECT * FROM citations').all()
    let num = 10
    let requ = +req.params.id
    let id = ((num * requ)-10)     
    // console.log("debut du req", id)
    // console.log("mon req params:",requ)
    const citations = db.prepare('SELECT * FROM citations LIMIT 10 OFFSET ?').all(id);
    //console.log("citat:",cita.length)
    let longueur = Math.ceil(((cita.length/10)))
    //console.log("ma limité:",longueur)
    
    if(requ >= longueur){
        requ = longueur-1
    }
    //citations.filter((element: any) => element.auteur_id === auteur.id)
    // if (citations.length % 10 > 1){
    //     document.createElement= `<button>Page Suivante</button>`
    // }
    //let isFounded = citations.some( elm => auteur.includes(elm) )

    // console.log("lalda",citations)
    // console.log("auteur:" , auteur)
    res.render('pages/admin-citations', {
        citations: citations,
        auteur: auteur,
        id : requ
    });
}

static readAll(req: Request, res: Response)
{
    const db = req.app.locals.db;
    const auteur = db.prepare('SELECT * FROM auteur').all()
    
    // console.log("debut du req", id)
    // console.log("mon req params:",requ)

    //console.log("ma limité:",longueur)
    
    
    //citations.filter((element: any) => element.auteur_id === auteur.id)
    // if (citations.length % 10 > 1){
    //     document.createElement= `<button>Page Suivante</button>`
    // }
    //let isFounded = citations.some( elm => auteur.includes(elm) )

    // console.log("lalda",citations)
    // console.log("auteur:" , auteur)
    res.render('pages/admin-auteur', {
        auteur: auteur,
    });
}






}